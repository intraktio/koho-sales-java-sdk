package com.intraktio.kohosales.config;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class ServiceConfig extends HashMap<String, String> { 
	public ServiceConfig() {
		super();
		this.putAll(getConfig());
	}

	protected String getConfigFilePath() {
		String filePath = System.getenv("KOHOSALES_CONFIG");
		if (filePath == null || filePath.isEmpty()) {
			throw new IllegalStateException("Environment variable KOHOSALES_CONFIG not set.");
		}
		return filePath;
	}

	private HashMap<String, String> getConfig() {
		try {
			byte[] mapData = Files.readAllBytes(Paths.get(getConfigFilePath()));
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.readValue(mapData, HashMap.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
