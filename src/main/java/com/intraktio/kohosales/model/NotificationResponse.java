package com.intraktio.kohosales.model;

import lombok.Getter;
import lombok.Setter;

public class NotificationResponse {

    @Getter @Setter
    public boolean success;

    @Getter @Setter
    public String error;

}

