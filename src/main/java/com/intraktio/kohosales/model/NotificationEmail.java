package com.intraktio.kohosales.model;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

public class NotificationEmail {

    @Getter @Setter
	private String from;

    @Getter @Setter
	private String[] to;

    @Getter @Setter
	private String[] cc;

    @Getter @Setter
	private String[] bcc;

    @Getter @Setter
	private String subject;

    @Getter @Setter
	private String content;

	public void setTo(String... to) {
		this.to = to;
	}

	public void setCc(String... cc) {
		this.cc = cc;
	}

	public void setBcc(String... bcc) {
		this.bcc = bcc;
	}

}
