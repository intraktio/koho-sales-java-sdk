package com.intraktio.kohosales.model;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

public class NotificationSms {

    @Getter @Setter
	private String from;

    @Getter @Setter
	private String[] to;

    @Getter @Setter
	private String content;

	public void setTo(String... to) {
		this.to = to;
	}
	
}
