package com.intraktio.kohosales.service;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import org.json.JSONObject;

import com.intraktio.kohosales.config.ServiceConfig;
import com.intraktio.kohosales.model.NotificationEmail;
import com.intraktio.kohosales.model.NotificationResponse;
import com.intraktio.kohosales.model.NotificationSms;
import org.apache.commons.codec.Charsets;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class NotificationService {

    protected final HashMap<String, String> config;

    public NotificationService() {
	this(new ServiceConfig());
    }

    public NotificationService(ServiceConfig config) {
	this.config = config;
    }

    public NotificationResponse sendEmail(NotificationEmail email) {
	String token = config.get("token");
	String url = config.get("url") + "/sendEmail";
	return makeRequest(url, token, new JSONObject(email));
    }

    public NotificationResponse sendSms(NotificationSms sms) {
	String token = config.get("token");
	String url = config.get("url") + "/sendSms";
	return makeRequest(url, token, new JSONObject(sms));
    }

    protected NotificationResponse makeRequest(String url, String authToken, JSONObject data) {
	HttpClient httpClient = HttpClientBuilder.create().build();
	try {
	    HttpPost req = new HttpPost(url);
	    req.addHeader("Content-Type", "application/json");
	    req.addHeader("authorization", authToken);
	    StringEntity entity = new StringEntity(data.toString(), "utf-8");
	    req.setEntity(entity);
	    return toNotificationResponse(getJsonObjectFromResponse(httpClient.execute(req)));
	} catch(Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }

    private NotificationResponse toNotificationResponse(JSONObject json) {
	NotificationResponse resp = new NotificationResponse();
	resp.setSuccess(json.getBoolean("Success"));
	if (json.has("Error")) {
	    resp.setError(json.getString("Error"));
	}
	return resp;
    }

    private JSONObject getJsonObjectFromResponse(HttpResponse response) {
	HttpEntity entity = response.getEntity();
	Header encodingHeader = entity.getContentEncoding();

	Charset encoding = encodingHeader == null ? StandardCharsets.UTF_8:
	    Charsets.toCharset(encodingHeader.getValue());
	try {
	    return new JSONObject(EntityUtils.toString(entity, StandardCharsets.UTF_8));
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	} 
    }

}
