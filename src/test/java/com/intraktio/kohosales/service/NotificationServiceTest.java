package com.intraktio.kohosales.service;

import com.intraktio.kohosales.config.ServiceConfig;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class NotificationServiceTest {

    private NotificationService notifService;
    private MockServiceConfig config;

    @Before
    public void before() {
        config = new MockServiceConfig();
        notifService = new NotificationService(config);
    }

    @Test
    public void testConfigInit() {
        assertNotNull(config.get("token"));
    }

    class MockServiceConfig extends ServiceConfig {
        @Override
        protected String getConfigFilePath() {
            return this.getClass().getClassLoader().getResource("io-notifications.json").toString().substring(5);
        }
    }

}
